CREATE TABLE IF NOT EXISTS `livestream_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreignid` varchar(32) NOT NULL,
  `category` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `imageheight` int(11) NOT NULL,
  `imagewidth` int(11) NOT NULL,
  `cardstyle` varchar(2) NOT NULL DEFAULT 'w1',
  `link` text NOT NULL,
  `views` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `foreignid` (`foreignid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;