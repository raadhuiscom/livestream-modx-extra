<?php
// Infinite Social Wall
// 10.8.2012
// Philip Bjorge
// https://github.com/philipbjorge/Infinite-Social-Wall
// Dual MIT/BSD License
// http://modernizr.com/license/
//
//
require_once('config.php');
require_once('lib/simplepie_1.3.1.mini.php');

//load modx externally
require_once '../config.core.php';
require_once MODX_CORE_PATH.'model/modx/modx.class.php';
$modx = new modX();
$modx->initialize('web');
$modx->getService('error','error.modError', '', '');

//haal site_start (dashboard)
$site_start = $modx->getOption('site_start');
$r = $modx->getObject('modResource', $site_start);
$feeds_json = $r->getTVValue('feeds');

$feeds = json_decode($feeds_json);

// ************************************************
//
//   HELPERS
//
// ************************************************
function replace_api_keywords($feeds) {
	$urls = array();
	$categories = array();
	foreach ($feeds as $feed) {
		$url = $feed->feed;
		// Replace our {XXX} variables in our URL strings
		$categories[$url] = $feed->category;
		$urls[] = $url;
	}
	return array($urls, $categories);
}

function regexp_url_search($r) {
	// Modified from http://www.bytemycode.com/snippets/snippet/602/
	$maxurl_len = 35;
	$url = $r[0];
	$offset1 = ceil(0.65 * $maxurl_len) - 2;
	$offset2 = ceil(0.30 * $maxurl_len) - 1;
	if ($maxurl_len AND strlen($url) > $maxurl_len)
		$urltext = substr($url, 0, $offset1) . '...' . substr($url, -$offset2);
	else
		$urltext = $url;		
	$text = '<a href="'. $url .'" title="'. $url .'">'. $urltext .'</a>';
	return $text;
}

function autoLinkUrls($text) {
	// Callback preg_replace found on 
	// http://stackoverflow.com/questions/9102003/php-auto-detect-links-and-put-them-into-a-tag-except-when-they-are-already
	$text = preg_replace_callback('#(?<![href|src]\=[\'"]|=)(https?|ftp|file)://[-A-Za-z0-9+&@\#/%()?=~_|$!:,.;]*[-A-Za-z0-9+&@\#/%()=~_|$]#', 'regexp_url_search', $text);
	return $text;
}

function clean_stristr($title, $content) {
	// Strips out tags (after converting special chars), whitespace and trailing ellipses (...s).
	$title = trim(preg_replace('/\s+/', '', strip_tags(htmlspecialchars_decode($title, ENT_QUOTES))), ".");
	$content = trim(preg_replace('/\s+/', '', strip_tags(htmlspecialchars_decode($content, ENT_QUOTES))), ".");
	return mb_stristr($content, $title) !== FALSE;
}

function truncate_DB($mysqli, $mysql_table, $all){

	if($all=='yes') {
		$stmt = $mysqli->prepare("TRUNCATE TABLE {$mysql_table}");
	} else {
		$stmt = $mysqli->prepare("TRUNCATE TABLE {$mysql_table} where isactive = 1");
	}

	$stmt->execute();
}

function setinactive_DB($mysqli, $mysql_table, $id){
	$query = "UPDATE {$mysql_table} SET isactive = 0 where id = ".$id;
				if ($ding = mysqli_prepare($mysqli, $query)) {
					if(!mysqli_stmt_execute($ding)){
						echo 'error!';
					};
					/* close statement */
    					mysqli_stmt_close($ding);	
				}
}

function getTimestamp($mysqli, $mysql_table){
	if ($result = mysqli_query($mysqli, "SELECT UNIX_TIMESTAMP(CURRENT_TIMESTAMP)")) {
    		while ($row = mysqli_fetch_row($result)) {
        		echo $row[0];
    		}
    		mysqli_free_result($result);
	}
}

function imagetype ( $image )
{
  if ( function_exists( 'exif_imagetype' ) )
    return exif_imagetype( $image);

  $r = getimagesize( $image );
  return $r[2];
}

// **************************************************************************
//
//   UPDATE LOGIC
//
// **************************************************************************
function update_DB($feeds, $mysqli, $mysql_table) {
	// Creates our url list and our URL => category map.
	list($urls, $categories) = replace_api_keywords($feeds);

	
	// Initialize SimplePie
	$feed = new SimplePie();
	$feed->set_feed_url($urls);
	$feed->set_cache_location('./cache');
	$feed->set_cache_duration(900);	// 15 minute cache
	$feed->init();

	// Initialize a prepared SQL statement for saving our social stream.
	$stmt = $mysqli->prepare("INSERT INTO {$mysql_table} (foreignid, category, title, content, image, link, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
	
	$stmt->bind_param('sssssss', $foreignid, $category, $title, $content, $image, $link, $date);

	foreach($feed->get_items() as $item) {
		
		$foreignid = $item->get_id(true);

		$category = $categories[$item->get_feed()->feed_url];

		$title = $item->get_title();
		
		$content = $item->get_content();

		$image = '';

		$link = $item->get_link();

		$date = $item->get_date("Y-m-d\TH:i:sO");
		
		if ($stmt->execute()) {
			// New Entry, so we render it as html.
		
			$remoteImage = array();

			//get all images with DOMDocument
			$doc = new DOMDocument();
			@$doc->loadHTML($item->get_content());
			$tags = $doc->getElementsByTagName('img');
			//put them in an array
			foreach($tags as $tag){
				$src = $tag->getAttribute('src');
				$remoteImage[]['src'] = $src;
			}

			//images found?
			if(count($remoteImage)>0) {
			
				$url = $remoteImage[0]['src'];
					
				$dir = "cache/";
				$newimage = $dir . basename($url);
			
				file_put_contents($newimage, file_get_contents($url));

				$image = $newimage;

				if(imagetype($newimage) == IMAGETYPE_GIF) {
					$image = 'cache/'.md5($newimage).'.gif';
					rename($newimage,$image);
				} elseif (imagetype($newimage) == IMAGETYPE_JPEG) {
					$image = 'cache/'.md5($newimage).'.jpg';
					rename($newimage,$image);
				} elseif (imagetype($newimage) == IMAGETYPE_PNG) {
					$image = 'cache/'.md5($newimage).'.png';
					rename($newimage,$image);
				}
				
				$width = 0;

				if(!empty($image)) {
					list($width, $height, $type, $attr) = getimagesize($image);
				}

				if($width > 400) {
					$style = 'w2';
				} elseif(strlen($content)<10 && $width>0) {
					$style = 'w4';
				} elseif($width>0) {
					$style = 'w3';
				} else {
					$style = 'w1';
				}
				
				if($category=='flickr'){
					$style = 'w4';
				}
	
				$content = preg_replace("/<img[^>]+\>/i", "", $item->get_content());
		
				$query = "UPDATE {$mysql_table} SET cardstyle = '".$style."', content = '".mysql_escape_string($content)."', imagewidth = '".$width."', imageheight = '".$height."',image = '".$image."' WHERE id = ".$stmt->insert_id;

				echo $query;
				echo "\n";

				if ($ding = mysqli_prepare($mysqli, $query)) {
					mysqli_stmt_execute($ding);
					/* close statement */
    					mysqli_stmt_close($ding);	
				}
			}


		}
	}

	$mysqli->close();
	exit;
}
 
// **************************************************************************
//
//   GET/PAGE LOGIC
//
// **************************************************************************
 
// Pagination variable.

if (!isset($_GET['p'])) {
	$p = 1;
} else if ($_GET['p'] == "update") {
	update_DB($feeds, $mysqli, $mysql_table);
} else if ($_GET['p'] == "truncateall") {
	truncate_DB($mysqli, $mysql_table,'yes');
} else if ($_GET['p'] == "truncate") {
	truncate_DB($mysqli, $mysql_table,'no');
} else if ($_GET['p'] == "setinactive") {
	setinactive_DB($mysqli, $mysql_table, $_GET['id']);
} else if ($_GET['p'] == "gettimestamp") {
	getTimestamp($mysqli, $mysql_table);
}
?>
