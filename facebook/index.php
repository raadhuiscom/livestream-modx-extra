<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();



define('FACEBOOK_SDK_V4_SRC_DIR', 'facebook-php-sdk-v4-4.0-dev/src/Facebook/');
require __DIR__ . '/facebook-php-sdk-v4-4.0-dev/autoload.php';

// Make sure to load the Facebook SDK for PHP via composer or manually

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
// add other classes you plan to use, e.g.:
 use Facebook\FacebookRequest;
 use Facebook\GraphUser;
// use Facebook\FacebookRequestException;
use Facebook\GraphObject;


FacebookSession::setDefaultApplication('470927279747872', '2a273381ae45f245ce27671b26c4fd29');

// $session = false;

// Add `use Facebook\FacebookRedirectLoginHelper;` to top of file
$helper = new FacebookRedirectLoginHelper($_SERVER[‘SCRIPT_FILENAME’]);

//$accessToken = 'AQD2qog-17CdzGt06W7e9XQ2nhN59MXk-psUIpUcbL6ljd1pjHGa_ioGG32Qyafip7cmarvxQw2WBhMMYHyNvlLXJfRoLTOaotReqEEIzNhgOLBgmSyjDmm849rVd_jeXWBfdSgrdcdmH7-tJA8-dLhSoI9ACBnxHayFepcxRRPoM_muLd3CRyIOqRidR9GZYwr8NCjVPI3inwIii8Mzq0fSbbmPUeyXCi6wpJkIDFOZWzEVc6-owWMiw8kTkUmqw9X9nnWeVbVeb7fPok_LY2ECD1UquCS2pc-Gj9y9Q2Op61ehk9BKDT1jgfL2eUH0E_kPVgnY1vLHtseGY1RT8opU';

//$session = new FacebookSession($accessToken);
$session = FacebookSession::newAppSession();

/*
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
//i	header('Location ' .$loginUrl);
*/
if (isset($session)) {
  // Logged in
  try {
// graph api request for user data
  $screen_name = $_GET['facebookid'];
    //Max title length in characters, will trim on word boundary
    $title_length = 70;
  $request = new FacebookRequest( $session, 'GET', '/'.$screen_name.'/feed' );
  $response = $request->execute();
  // get response
  $obj = $response->getGraphObject();
	$return = $obj->asArray();
    $now          = date("D, d M Y H:i:s T"); 
 
    $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?> 
        <rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\"> 
            <channel> 
                <title>".$screen_name."</title> 
                <link>http://www.facebook.com/".$screen_name."</link> 
                <description> </description> 
                <pubDate>$now</pubDate> 
                <lastBuildDate>$now</lastBuildDate> 
                "; 
    foreach ($return as $lines){ 
    foreach ($lines as $line){ 
	$line = json_decode(json_encode($line), FALSE);	

	if(!isset($line->message)){
		continue;
		$m = '';	
	} else {
		$m = $line->message;
	}

	//die();

	//var_dump($line);

        //Catch facebook posts with no links and set it to the page url 
        if (!isset($line->link) && empty($line->link)) { 
	    $line->link = new StdClass;
            $line->link = "http://www.facebook.com/".$screen_name."#"; 
        } 
 
        //Check for titles longer than the specified length, trims on word end and adds ellipses 
        if (strlen($m) > $title_length) {  
            $linetitle = preg_replace('/\s+?(\S+)?$/', '', substr($m, 0, $title_length)) . ' ...'; 
        } else { 
            $linetitle = $m; 
        } 
 
        $output .= "<item><title><![CDATA[".htmlspecialchars(strip_tags($linetitle),ENT_COMPAT,'utf-8')." ]]></title> 
            <link>".utf8_encode(htmlentities(strip_tags($line->link),ENT_COMPAT,'utf-8'))."</link> 
        ";
	if (isset($line->picture) && !empty($line->picture)) { 
        	$output .=    "<description><![CDATA[<img src='".$line->picture."'>".htmlspecialchars(strip_tags($m),ENT_COMPAT,'utf-8')." ]]></description> ";
       	} else {
        	$output .=    "<description><![CDATA[".htmlspecialchars(strip_tags($m),ENT_COMPAT,'utf-8')." ]]></description> ";
	} 
        $output .=    "<author>".htmlentities($line->from->name)."</author> 
            <pubDate>".date("D, d M Y H:i:s T",strtotime($line->created_time))."</pubDate> 
            <guid>".utf8_encode(htmlentities(strip_tags($line->link),ENT_COMPAT,'utf-8'))."</guid> 
            </item> 
            "; 
    } }
    $output .= "</channel> 
                </rss>"; 
    header("Content-Type: application/rss+xml"); 
    echo $output; 

  } catch(FacebookRequestException $e) {
    echo "Exception occured, code: " . $e->getCode();
    echo " with message: " . $e->getMessage();
  }   
} else {
  echo '<a href="' . $helper->getLoginUrl() . '">Login</a>';
}




//$session = new FacebookSession('access token here');











?>
