<?php

$client_id = ''; // https://instagram.com/developer/clients/manage/

if(isset($_GET['q'])) {
	$d = json_decode(file_get_contents('https://api.instagram.com/v1/tags/'.$_GET['q'].'/media/recent?client_id='.$client_id));
} elseif (isset($_GET['lat']) & isset($_GET['lng'])) {

	$d = json_decode(file_get_contents('https://api.instagram.com/v1/locations/search/?lat='.$_GET['lat'].'&lng='.$_GET['lng'].'&client_id='.$client_id));

	$d = json_decode(file_get_contents('https://api.instagram.com/v1/locations/401670/media/recent?client_id='.$client_id));
	// kaasplein 401670
// klimmen 434928899
//881820264
	echo "<pre>";
	print_r($d);
die();
} else {
	die('sowwy');
}

$now = date("D, d M Y H:i:s T");
$now = rfc822Date($now);
$link = htmlspecialchars('http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING']);

header("Content-Type: application/xml; charset=UTF-8");

?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Instagram hashtag <?php echo $_GET['q']; ?></title>
		<link><?php echo $link; ?></link>
		<atom:link href="<?php echo $link; ?>" rel="self" type="application/rss+xml" />
		<description><?php echo $description; ?></description>
		<pubDate><?php echo $now; ?></pubDate>
		<lastBuildDate><?php echo $now; ?></lastBuildDate>
<?php
$tweets = ($usage == 'hashtag') ? $return->statuses : $return;
foreach ($d->data as $line){

	$title= htmlspecialchars(htmlspecialchars_decode($line->user->full_name .': '. $line->caption->text));
	$description= $title;//htmlspecialchars(htmlspecialchars_decode(strip_tags($line->text)));
	$url = htmlspecialchars($line->link);
	$image = $line->images->standard_resolution->url;
	$created_at = rfc822Date($line->created_time);
?>
		<item>
			<title><?php echo $title; ?></title>
			<description>
			<![CDATA[
			<?php echo $description;?>
				<img src="<?php echo $image; ?>">
		]]></description>
			<pubDate><?php echo $created_at ?></pubDate>
			<guid><?php echo $url; ?></guid>
			<link><?php echo $url; ?></link>
		</item>
<?php
}
?>
</channel>
</rss>
<?php
function rfc822Date($str){
	if(!isValidTimeStamp($str)) {
		$timestamp = strtotime($str);
		return date(DATE_RSS, $timestamp);
	}
	return date(DATE_RSS, $str);
}

function isValidTimeStamp($timestamp)
{
    return ((string) (int) $timestamp === $timestamp) 
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
}

?>
