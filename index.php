<?php 
require_once('config.php'); ?>
<html>
<head>
 <title><?php echo $theme?> Live Stream</title>
<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <style type="text/css"> .clear { zoom: 1;display: block;} </style> <![endif]-->
    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="livestream-modx-frontend/stylesheets/base.css">
    <link rel="stylesheet" type="text/css" href="livestream-modx-frontend/stylesheets/skeleton.css">
    <link rel="stylesheet" type="text/css" href="livestream-modx-frontend/stylesheets/layout.css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="livestream-modx-frontend/images/favicon.ico">
    <link rel="apple-touch-icon" href="livestream-modx-frontend/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="livestream-modx-frontend/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="livestream-modx-frontend/images/apple-touch-icon-114x114.png">
    <!-- Google Font
	================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="overlay-wrapper">
    <div class="overlay twitter">
        <img src="livestream-modx-frontend/images/<?php echo $theme?>-large.jpg" class="overlay-image"/>

        <div class="overlay-info">
            <div class="social-icon-wrapper">
                <div class="social-icon"></div>
            </div>
            <div class="details">
                <span class="time">1 uur geleden</span><span class="views">373</span>
            </div>
            <a href="#" class="overlay-close"><img src="livestream-modx-frontend/images/close.svg"/></a>
            <hr>
            <div class="description">

            </div>
            <div class="author">
                <img class="author-avatar" src="livestream-modx-frontend/images/author-avatar.jpg"/><span
                    class="author-name">De Stadomroeper<br/></span><a href="#">@Gebruiker2</a>
            </div>
        </div>
    </div>
</div>

<div class="header" style="background: url(livestream-modx-frontend/images/<?php echo $theme?>-header.jpg); background-size: cover;">
    <img id="logo" src="livestream-modx-frontend/images/<?php echo $theme?>-logo.png" class="scale-with-grid"/>
</div>

<div class="container">
    <div class="sixteen columns">
        <div id="container" class="isotope variable-sizes clearfix infinite-scrolling">
		<?php include('get_stream.php'); ?>
  		<div id="new-content"></div>
	</div>
    </div>
</div>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script src="livestream-modx-frontend/js/jquery.isotope.min.js"></script>
  <script src="livestream-modx-frontend/js/jquery.infinitescroll.min.js"></script>
  <script src="livestream-modx-frontend/js/jquery.timeago.js"></script>

  <script src="livestream-modx-frontend/js/app.js"></script>
</body>
</html>
