# global installation

```
#!bash
git clone https://bitbucket.org/raadhuiscom/livestream-modx-extra.git livestream
cp config.php.default config.php
```

# Instagram

Register an API key on https://instagram.com/developer/clients/manage/
Put this key into index.php and you’re done.

/livestream/instagram/?q=icgalkmaar2015

# Facebook

If it works, fine. Otherwise use sierag.nl/fb/. Alternative read the facebook api documentation how to install this tool.
Good to know: Requires PHP 5.4 to work

### Using the Facebook feed example

Simply by /livestream/facebook/?facebookid=1669060316652576


# Flickr

Flickr has a nice rss feed api thing by itself:
Just use https://api.flickr.com/services/feeds/photos_public.gne?id=132670191@N04
or https://api.flickr.com/services/feeds/photos_public.gne?tags=icgalkmaar

Lowercase or capitals are ignored.


# Twitter

You will first have to register a new twitter app on https://apps.twitter.com/. Then you’re able to retrieve twitter api keys

```
#!bash
cd twitter
cp twitter_auth.php.default to twitter_auth.php
```

Now edit twitter_auth.php and fill in your twitter api keys and secrets.


### Using the twitter feed example

On this way you can get an account. Parameter screen_name should be used:

/livestream/twitter/twitter_json_to_rss.php?screen_name=ICG2015Alkmaar

Want to use a search? Then use the q parameter like this:

/livestream/twitter/twitter_json_to_rss.php?q=icgalkmaar
/livestream/twitter/twitter_json_to_rss.php?q=International%20Children%27s%20Games

You can also use %20 (spaces) to search on ‘International Children’s Games’ and ‘International Games Children’s’ and ‘Children’s International Games’. These all work. 

Lowercase or capitals are ignored.