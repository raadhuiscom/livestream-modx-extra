<?php
/*
	fetches a given tweet, and renders an html page using the tweet's content. Useful to control how facebook publishes a link to the tweet (if just using the twitter status url, the description is Twitter's default description meta tag, yuk!).
*/

error_reporting(E_WARNING | E_ERROR);
ini_set('display_errors',1);

$tid = null;

if(!isset($_GET['tid'])){
	die("nope, sorry, no tweet specified");
}

$tid = trim($_GET['tid']);

require 'tmhOAuth/tmhOAuth.php';
require 'twitter_auth.php';

$tmhOAuth = new tmhOAuth($twitter_auth);

$url = "/1.1/statuses/show.json";
$options = array(
	'id'=> $tid,
);

// DO !

$code = $tmhOAuth->request('GET', $tmhOAuth->url($url), $options);
$tweet = json_decode($tmhOAuth->response['response'], false);

$tweet->title= htmlspecialchars(htmlspecialchars_decode($tweet->user->name.": ".strip_tags($tweet->text)));
$tweet->description= htmlspecialchars(htmlspecialchars_decode(strip_tags($tweet->text)));
$tweet->descriptionhtml = find_hashtags(find_links($description));

$tweet->url = htmlspecialchars("https://twitter.com/".$tweet->user->screen_name."/statuses/".$tweet->id_str);;
$tweet->image = (strlen($tweet->entities->media[0]->media_url)>0) ? htmlspecialchars($tweet->entities->media[0]->media_url) : null;
$datetime = new DateTime($tweet->created_at);
$datetime->setTimezone(new DateTimeZone('Europe/Brussels'));

$tweet->created_at = $datetime->format(DATE_RFC822);
$tweet->profile_url= $tweet->user->profile_image_url;

header('Content-Type: application/json');
echo json_encode($tweet);

function find_links($text){
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        // make the urls hyper links
        return  (preg_match($reg_exUrl, $text, $url)) ? preg_replace($reg_exUrl, '<a href="'.$url[0].'">'.$url[0].'</a> ', $text) : $text;
}

function find_hashtags($text){
// https://twitter.com/search?q=%23dwmaj&src=hash
        return preg_replace("/#(\w+)/i", "<a href=\"http://twitter.com/search?q=%23$1\">#$1</a>", $text);
}

?>
