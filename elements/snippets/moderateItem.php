<?php
/* moderateItem
Deze snippet zet een placeholder voor manager rechten.
En hij vangt op of een item moet worden verwijderd.



*/
//current id of resource
$id = $modx->resource->get('id');

//retrieve current auth user
$user = $modx->getUser();
//check for membership of admin group
if( $user->isMember('Administrator')){
	$modx->setPlaceholder('isAdmin',1);
	$isAdmin = true;
}

//is er geklikt?
if(!empty($_GET['rid']) && $isAdmin==true){
	$rid = intval($_GET['rid']);
	
	//load Package
$path = MODX_BASE_PATH . 'livestream/livestream_xpdo/';
$result = $modx->addPackage('livestream_xpdo',$path .
    'model/','livestream_');
 
	if (! $result) {
	    return 'failed to add package';
	}
	
	$ridObject = $modx->getObject('Items',array(
		'id' => $rid, 
	));
	$ridObject->set('isactive',0);
	if($ridObject->save()){
		$url = $modx->makeUrl($id);
		$modx->sendRedirect($url);
	}
}