<?php
/* getItems
Deze snippet haalt alle social items op en geeft deze weer op basis van url
Usage: 
[[!getItems? 
	&tpl=`` #this is the template for each item
	&tplAlt=`` #this is the template for each alternative item (odd)
	&noResults=`` #this is the text display if there are no items
	&toPlaceholder=`` #when used the results get outputted to a placeholder with that name
	&limit=`` #limit the  results by this for each page
	&offset=`` #offset to begin at
	&sortby=`` #sort by any field
	&sortdir=`` #sort direction ASC or DESC
	&activeOnly=`` #show active items only, defaults to 1
]]
*/

//error reporting
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

//define options (and defaults)
$tpl = $modx->getOption('tpl', $scriptProperties, 'item_tpl');
$tplAlt = $modx->getOption('tplAlt', $scriptProperties, '');
$noResults = $modx->getOption('noResults', $scriptProperties, 'No posts found...');
$toPlaceholder = $modx->getOption('toPlaceholder', $scriptProperties, '');
$sortby = $modx->getOption('sortby', $scriptProperties, 'date');
$sortdir = $modx->getOption('sortdir', $scriptProperties, 'DESC');
$activeOnly = $modx->getOption('activeOnly', $scriptProperties, 1);
$onlyThese = isset($_GET['onlythese']) ? $_GET['onlythese'] : '';
$afterThis = isset($_GET['afterthis']) ? $_GET['afterthis'] : '';

//get specific items part
$onlyThese = explode(';',$onlyThese);
foreach($onlyThese as $key=>$onlyThis){
	if(!ctype_alnum($onlyThis)){
		$onlyThese[$key] = preg_replace("/[^A-Za-z0-9]/","",$onlyThis);
	}
}
$onlyThese = array_filter($onlyThese);

//get items by timestamp
$afterThis = intval($afterThis);
$afterThisDate = date('Y-m-d H:i:s',$afterThis);

//pagination options (and defaults)
$totalVar = $modx->getOption('totalVar', $scriptProperties, 'total');
$limit = $modx->getOption('limit',$scriptProperties,40);
$offset = $modx->getOption('offset',$scriptProperties,0);

//used variables
$items;
$item;
$output ='';
$c;
$tplCurr = '';

//load Package
$path = MODX_BASE_PATH . 'livestream/livestream_xpdo/';
$result = $modx->addPackage('livestream_xpdo',$path .
    'model/','livestream_');
 
if (! $result) {
    return 'failed to add package';
}

//check totaal aantal
$items = $modx->getCollection('Items');

//return total for pagination
$total = count($items);
$modx->setPlaceholder($totalVar,$total);

$c = $modx->newQuery('Items');

//limit by given ID's
if(count($onlyThese)>0){
	$c->where(array(
    'foreignid:IN' => $onlyThese));
}

//only return items after given timestamp
if(!empty($afterThis)){
	$c->where(array(
    'created:>=' => $afterThisDate));
}

//isactive items only
if($activeOnly==1){
	$c->where(array(
		'isactive' => 1,
	));
}

$c->sortby($sortby,$sortdir);
//define limit, offset voor pagination
$c->limit($limit,$offset);
$items = $modx->getCollection('Items',$c);

//iterate for templating
foreach($items as $item){
	$placeholders = array(
		'id' => $item->id,
		'foreignid' => $item->foreignid,
		'category' => $item->category,
		'title' => $item->title,
		'content' => $item->content,
		'image' => $item->image,
		'imageheight' => $item->imageheight,
		'imagewidth' => $item->imagewidth,
		'date' => $item->date,
		'views' => $item->views,
		'cardstyle' => $item->cardstyle,
		'created' => $item->created			
	);

	//alternatieve template inladen
	if(!empty($tplAlt)){
		if($tplCurr == $tpl){
			$tplCurr =  $tplAlt;
		}else{
			$tplCurr = $tpl;
		}
	}else{
		$tplCurr = $tpl;
	}

	//ophalen en verwerken template met alle gevonden velden
	$output .= $modx->getChunk($tplCurr,$placeholders).'
	';
}

//check if empty then display $noResults text
if(empty($output)){
	$output = $noResults;
}

//als output niet leeg dan output naar placeholder of direct
if(!empty($toPlaceholder)){
	$modx->toPlaceholder($toPlaceholder,$output);
}else{
	return $output;
}