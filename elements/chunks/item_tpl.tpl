<div class="item [[+cardstyle:default=`w1`]] [[+category]]" data-category="[[+category]]">
    <div class="content"> 
        <div class="item-hover"> 
            <img src="livestream/livestream-modx-frontend/images/open.svg" class="item-open"/> 
	    [[+isAdmin:is=`1`:then=`<br /><a style="" class='setinactive' data-id='[[+id]]' href="/livestream/get_stream.php?p=setinactive&id=[[+id]]" target='_blank'>
<img src="livestream/livestream-modx-frontend/images/trash.png" class="" width='64px'/>
		</a>`]]
        </div> 
        [[+image:notempty=`<div class="background" style="background: url('livestream/[[+image]]') center center no-repeat; background-size: cover;"></div>`]]
        [[+cardstyle:is=`w1`:then=`<div class="gradient-white"></div>`]]
        [[+cardstyle:is=`w3`:then=`<div class="gradient-white"></div>`]]
        <div class="info"> 
            <div class="social-icon-wrapper"> 
                <div class="social-icon"></div> 
            </div> 
            <div class="details">
    			<time class="time" datetime="[[+date]]" pubdate>[[+date]]</time>
            </div> 
            <div class="description"> 
	    	[[+content]]

	    </div>
        </div> 
    </div> 
</div>
